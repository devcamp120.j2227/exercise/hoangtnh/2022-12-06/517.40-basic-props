import { Component } from "react";

class OutputBody extends Component{
    render(){
        const {outputMessageProp, catImgProp} = this.props
        return (
            <div style={{marginTop:"5px"}}>
                {outputMessageProp? <p style={{textAlign:"center",color:"blue"}}>meow not found :)</p>: null }
                {catImgProp ? <img alt="imgCat" src = "https://s3-us-west-2.amazonaws.com/s.cdpn.io/541117/cat.jpg" style={{width:"100",display:"flex",margin:"auto" }}/>: null }
            </div>
        )
    }
}
export default OutputBody;