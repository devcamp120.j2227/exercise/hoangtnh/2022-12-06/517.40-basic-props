import { Component } from "react";

class InputBody extends Component{
    onInputChangeHandler = (event) =>{
        console.log("Kind:", event.target.value);
        console.log(this.props);
        const {inputMessageChangeHandlerProp} = this.props;
        inputMessageChangeHandlerProp(event.target.value);
    }
    render(){
        const {inputMessageProp} = this.props;
        return(
            <div>
                <input style={{display:"flex", margin: "auto", marginTop:"20px"}} placeholder="write your kind pet in here" onChange={this.onInputChangeHandler} value={inputMessageProp}></input>
            </div>
        )
    }
}
export default InputBody;