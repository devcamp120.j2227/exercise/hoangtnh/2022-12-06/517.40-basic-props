import { Component } from "react";
import InputBody from "./inputKind/inputKind";
import OutputBody from "./outputKind/outputKind";
class Animal extends Component{
    constructor(props){
        super(props);
        this.state = {
            inputMessage: "",
            outputMessage: false,
            imgCat: false
        }
    }
    inputMessageChangeHandler = (value)=>{
        this.setState({
            inputMessage: value,
        })
        if(value === "cat"){
            this.setState({
                imgCat: true,
                outputMessage: false
            })
        }
        if(value !== "cat"){
            this.setState({
                imgCat: false,
                outputMessage: true,
            })
        }
        if(value ===""){
            this.setState({
                outputMessage:false
            })
        }
    }
    render(){
        return(
            <div>
                <InputBody inputMessageProp={this.state.inputMessage} inputMessageChangeHandlerProp={this.inputMessageChangeHandler} outputMessageChangeHandlerProp={this.outputMessageChangeHandler}/>
                <OutputBody catImgProp={this.state.imgCat} outputMessageProp={this.state.outputMessage}/>
            </div>
        )
    }
}
export default Animal;